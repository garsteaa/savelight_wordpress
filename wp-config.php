<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'savelight.test' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '2,P&yP1*BMcQ%YP_Z4-aRj1aC/;(KV]r$rH8Oqye<kMCYkPTk=pDWkA? YI-./Q9' );
define( 'SECURE_AUTH_KEY',  'IV&QZmUhv^/+j5U(V}(9/b@WZ}>9h>5{|5X^i;osA8Gwz2? @6]Kjwf-1ri83#X>' );
define( 'LOGGED_IN_KEY',    ']#*[@XJ_:5dV)YdT}msyE,$g@W11~IqhW-[Ba[0wY~-/JY.p,Qj.DPWo)]V8Zt^!' );
define( 'NONCE_KEY',        'ChquV (!(qu9yMqqLsuw!#/@6TSXlZM~r)YNdEn67:-C 9dxb`5~Znp(omfM)(Hc' );
define( 'AUTH_SALT',        'g}w)]Zg<k`~_>!{0`}M3a!W>::t%2dL3gOjpPY:#2)n+qD/_!.14*UqdreWuH_`u' );
define( 'SECURE_AUTH_SALT', 'P{R)NnYl&6!J!Q@9dWAi L/||,j]i$0E|}QAbZ>8+*Hh?9fuYvZ{?Wu;5x op5Ii' );
define( 'LOGGED_IN_SALT',   '0[Q0k]/=R(FSgt[_miGK]X)wo=~L6g5Ys3:Q^6: I38r^;jS0F h1NayL-*Qr!0W' );
define( 'NONCE_SALT',       'u/B<JlH1%+,u3R5Rv!|#WOXG,Y-i{yex9`Py&(Ea.Fl&vT5kmZ)jqiWngCQv85dI' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
